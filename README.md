# LoRa
## هدف این مقاله
در این مقاله از اینکه LoRa چیست و چگونه کار میکند تا چگونه با LoRa کار کنیم، گفته میشود.
## مدولاسیون LoRa چیست
لغت LoRa مخفف **Long Range** به معنای **برد بلند** است و واقعا هم شایسته چنین اسمی میباشد. چرا که برد آن در ارتباط بالن به زمین به بیش از 702Km نیز رسیده است! [لینک رکورد][7ccec0ce]

آقای Andreas Spiess هم رکورد ارتباط زمین به زمین LoRa را ثبت کردند که 212Km بود! [لینک رکورد][764309f3]

فرستنده-گیرنده های LPWAN(Low Power Wide Area Network) به فرستنده گیرنده هایی گفته میشود که توان مصرفی پایینی دارند و ناحیه پوشش دهی بالا. که LoRa و SigFox از معروف ترین LPWAN ها میباشند.

مدولاسیون LoRa قادر است با مصرف توان پایین، برد بالایی را پوشش دهد.
درمورد ناحیه پوشش دهی LoRa مقادیر متفاوتی ذکر شده اما حدود آن بدین صورت است که در مناطق شهری تا حدود 20Km و در فضای بازتری مانند مناطق روستایی به 40Km میرسد. شعاع تحت پوشش SigFox نیز در همین حدود و شاید کمی بیشتر است.

ازنظر طول عمر نیز LoRa و SigFox ادعا کرده اند که با یک باتری 1AA و البته با  نرخ داده پایین، در حدود 10 سال دوام میاورند و البته در این زمینه نیز بنظر میرسد SigFox طول عمر کمی بیشتری را مدعی باشد.

تراشه SX1278 که مورد تست و بررسی قرار گرفت ، میتواند از مدولاسیون LoRa استفاده کند. البته این تراشه میتواند از مدولاسیون های رایج دیگری مانند FSK, GFSK, MSK, GMSK نیز استفاده کند.

تراشه های SX1276/77/78/79 در فرکانس های متفاوتی میتوانند کار کنند – 315, 433, 868, 900 MHz ISM Band که این باندها در ایران بدون نیاز به مجوز هستند.

![فرکانس های کاری تراشه های SX127x](/img/1.png)

## برخی از امکانات SX127x که باید با آن آشنا باشید

چیپ های SX127x از مدولاسیون LoRa استفاده میکنند و دارای امکانات زیادی هستند، اما مهم ترین متغیر هایی که در پروژه شما تاثیر میگذارند عبارتند از:
-	Band Width (BW) یا طول باند
-	Spreading Factor (SF)
-	error Correction Rate (CR)

### Band Width (BW)

هرچه طول باند ارتباط شما بیشتر باشد، میتوانید نرخ داده بیشتری را انتقال دهید. اما توجه کنید که طول باند بیشتر به معنای کمتر شدن مسافت پوشش نیز میباشد.

### Spreading Factor

هرچه SF (فاکتور انتقال داده) کمتر باشد، سرعت انتقال اطلاعات بالاتر میرود. در SX127x شاهد هفت SF مختلف هستیم: SF 6,7,8,9,10,11,12

با هر بار افزایش عدد SF، سرعت انتقال داده نصف میشود، ولی قسمت خوب ماجرا این است که برد تحت پوشش افزایش میابد.

![نقش SF های مختلف در سرعت انتقال داده](/img/2.png)

در تصویر بالا میبینید که با هر بار افزایش SF، زمان ارسال یک سمبل دوبرابر میشود. (در مورد نحوه مدولاسیون توضیح خواهیم داد)

![تاثیر طول باند و SF بر حساسیت گیرنده](/img/3.png)

در جدول بالا که از DataSheet چیپ های SX127x آورده شده است، میبینید که با افزایش SF و همچنین کاهش طول باند، به حساسیت بهتری در سمت گیرنده میرسیم و این یعنی برد پوشش دهی آن بیشتر میشود

### error Correction Rate (CR)

تراشه های SX127x از تکنیک Error Correction یا تصحیح خطا استفاده میکنند تا بتوانند برد بیشتری را پوشش دهی کنند و ارتباط پایدارتری را بوجود آورند.

فرض بر این است که اطلاعاتی که گیرنده دریافت میکند با آنچه فرستنده میفرستد متفاوت خواهند بود و این یعنی خطا (به هر دلیلی که بوجود آید). اما راهکارهایی هست که میتوانیم این خطا را در سمت گیرنده تشخیص دهیم و حتی آن را تصحیح کنیم! نمونه ساده و البته تا حدی ناکارآمد برای تشخیص خطا، اضافه کردن بیت توازن است که احتمالا با آن آشنا هستید، واگرنه میتوانید از این [لینک][1d0ca629] استفاده کنید (البته فقط برای آشنایی). تراشه های SX127x نیز از الگوریتم تشخیص و تصحیح خطا استفاده میکنند که میتوانیم در آنها، نسبت تعداد بیت مربوط به تصحیح خطا به 4 بیت داده را از مقادیر مقابل انتخاب کنین: 4/5, 4/6, 4/7, 4/8 که مثلا در 4/7 به ازای هر 4 بیت داده، از 7 بیت برای تصحیح خطا استفاده میکنیم. مشخص است که هرچه این نسبت بیشتر باشد میتوانیم خطاهای بیشتری را تصحیح کنیم و این یعنی اینکه میتوانیم برد پوششی را بیشتر کنیم. البته باید توجه کنیم که با افزایش این نسبت، برای فرستادن مقدار مشخصی از اطلاعات به بیت های بیشتری نیازمندیم و این یعنی کم شدن سرعت انتقال داده.

در لینک مقابل نیز میتوانید توضیحاتی در این مورد را مشاهده کنید: [Error Correction Rate][21d7c236]

## مدولاسیون LoRa چگونه انجام میشود

قدرت چیپ های SX127x در مدولاسیون خاص آنهاست، یعنی LoRa.

ویدئو کوتاه و مفیدی در این مورد را میتوانید در لینک مقابل ببینید: [مدولاسیون LoRa][1d0be94d]

لینک مقابل نیز توضیح کاملی را درمورد آنتن ها و LoRa و LoRaWAN (توضیح خواهیم داد) میدهد که با اینکه طولانی است اما حتما ارزش دیدن را دارد، لذا مشاهده آن شدیدا توصیه میشود. قسمت مدولاسیون آن از دقیقه 34 شروع میشود: [LoRa crash course by Thomas Telkamp][ec4151cd]
  
درمورد توضیح مدولاسیون LoRa از ویدئو بالا استفاده میکنم لذا اگر آن را مشاهده کرده اید میتوانید از این بخش عبور کنید.

در این نوع مدولاسیون، از پهنای باند بیشتری نسبت به دیگر مدولاسیون ها برای انتقال داده با نرخ برابر استفاده میشود که خوب نیست اما LoRa به این ویژگی نیاز دارد تا ویژگی های خوبش را ارائه دهد.

در تصویر زیر 3 نوع مدولاسیون دیده میشود که سمت چپ همانند کلید روشن خاموش عمل میکند. چیزی همانند [کد مورس][21d7c237]. 

مدولاسیون میانی، همان Frequency Shift Keying (FSK) معروف میباشد که میبینیم وقتی با زمان جلو میرویم در یکی از دو فرکانس پایین یا بالا را قرار داریم که مثلا فرکانس کمتر میتواند نشانگر بیت 0 و فرکانس بیشتر نشانگر بیت 1 باشد.

اما در مدولاسیون سمت راست شکل خاصی را میبینیم که LoRa نامیده میشود. برای اینکه خطوط بهتر دیده شوند چند تای آنها را با قرمز نشان دادیم. میبینید که در ابتدا چند خط مورب و موازی وجود دارد که وقتی در حوزه زمان به آن نگاه کنیم، سیگنالی را میبینیم که فرکانس آن بصورت پیوسته تا حد معینی افزایش میابد و سپس از دوباره از فرکانس پایین شروع میکند به افزایش فرکانس.

اما در قسمتی که با * (ستاره) مشخص شده است میبینیم که یکنواختی این افزایش فرکانس از بین میرود و شاهد شیفت فرکانس هستیم. این ناپیوستگی ها داده را بوجود میاورند.

![چند نمونه از مدولاسیون ها](/img/4.png)

در تصویر زیر نیز مدولاسیون LoRa را مشاهده میکنید و میتوانید طول باند و پریود هر سمبل (که با SF متناسب است) را ببینید.

![مدولاسیون LoRa](/img/5.png)

از تصویر زیر میتوانید متوجه شوید که LoRa چگونه از این خطوط در حوزه فرکانس، به داده میرسد. در سمت گیرنده chirp هایی برعکس chirp های اولیه تولید میشوند (تصویر میانی – Inverse Chirp) و این chirp ها در سیگنال ورودی ضرب شده و خطوط سمت راست را میدهند و بطور شگفت انگیزی به داده میرسیم. میبینیم که شیفت فرکانس در سیگنال ورودی منجر به تغییر سمبل خروجی میشود و تعدادی از این سمبل ها میتوانند تشکیل بیت و داده را بدهند.

![مدولاسیون LoRa](/img/6.png)

در توضیح مدولاسیون LoRa به همین حد بسنده میکنیم و توضیحات اضافی (مثل دلیل مقاوم بودن این نوع مدولاسیون به نویز و ...) را برعهده ویدئو ذکر شده میگذاریم.

## چگونگی استفاده از تراشه SX127x در LoRa RA-02

ماژول LoRa RA-01/02 محصول شرکت AI-Thinker است که از تراشه SX1278 استفاده میکند و تنها چند المان جانبی در کنار آن قرار داده است.

![LoRa Ra-02](/img/7.jpg)

### چند نکته کاربردی

-	در استفاده از فرستنده گیرنده های RF دقت کنید که وجود مانع بین فرستنده و گیرنده، مشکل بزرگی است و خیلی از برد موثر آنها میکاهد. تا حد امکان آنها را در مکانهای مرتفع قرار دهید تا نتیجه بهتری بگیرید.
-	توجه کنید که آنتن هایتان مخصوص فرکانس کاری فرستنده-گیرنده تان باشد. اکثر آنتن ها در فرکانس خاصی رزونانس میکنند و در فرکانس های دیگر بد کار میکنند.
-	توجه کنید که فرستنده گیرنده تان از تغزیه مناسبی برخوردار باشد. چرا که معمولا فرستنده-گیرنده ها دارای پیک های جریانی هستند که باعث افت ولتاژ میشود و منبع تغزیه باید جوابگوی پیک های جریانی باشد تا افت ولتاژ به حداقل رسد. معمولا در نزدیکترین نقطه به تغزیه فرستنده-گیرنده، خازن تانتالیوم قرار میدهند تا انرژی کافی را برای پیک های جریانی را تامین کند. دلیل نزدیکی خازن با تغزیه این است که سیم یا track مابین این دو، میزان کمی خاصیت سلفی دارد. وقتی تغییرات ناگهانی در جریان داشته باشیم سلف افت ولتاژ را بوجود میاورد. در پیک های جریانی ما نیز سیم یا track مابین خازن و تغزیه افت ولتاژ را بوجود میاورند پس هرچه طول سیم یا track کمتر باشد بهتر است.

### کار با LoRa Ra-02

این ماژول از طریق پروتکل SPI با میکرو ارتباط برقرار میکند و دارای رجیستر هایی است که با خواندن و نوشتن در آنها میتوانیم به اهدافمان برسیم.

اما نمیخواهیم وارد جزئیات شویم و میتوانیم از کتابخانه ای که sandeepmistry  نوشته اند برای ارتباط آردوینو با LoRa Ra استفاده کنیم: [لینک GitHub][cabf28e5]

#### اتصالات آردوینو و LoRa Ra02

![SX127x Wiring](/img/8.png)

همانند جدول بالا اتصالات را برقرار کنید. البته برای این مثال اتصال DIO0 به پایه 2 که یکی از پایه های وقفه در آردوینو هست لازم نیست زیرا از این قابلیت استفاده نمیکنیم.

#### کد های ارتباط آردوینو با LoRa Ra

پس از دانلود و نصب کتابخانه ([آموزش نصب کتابخانه در آردوینو][84f53b1d]) برنامه آردوینو خود را باز کنید(اگر قبلا باز بود آن را بسته و دوباره باز کنید تا کتابخانه جدید اضافه شود). سپس از طریق File > Examples > LoRa > LoRaDuplex مثالی برای ارتباط دوطرفه بین ماژول های LoRa را باز کنید.

```cpp
/*
  LoRa Duplex communication

  Sends a message every half second, and polls continually
  for new incoming messages. Implements a one-byte addressing scheme,
  with 0xFF as the broadcast address.

  Uses readString() from Stream class to read payload. The Stream class'
  timeout may affect other functuons, like the radio's callback. For an

  created 28 April 2017
  by Tom Igoe
*/
#include <SPI.h>              // include libraries
#include <LoRa.h>

const int csPin = 7;          // LoRa radio chip select
const int resetPin = 6;       // LoRa radio reset
const int irqPin = 1;         // change for your board; must be a hardware interrupt pin

String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0xBB;     // address of this device
byte destination = 0xFF;      // destination to send to
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends

void setup() {
  Serial.begin(9600);                   // initialize serial
  while (!Serial);

  Serial.println("LoRa Duplex");

  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

  if (!LoRa.begin(915E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
}

void loop() {
  if (millis() - lastSendTime > interval) {
    String message = "HeLoRa World!";   // send a message
    sendMessage(message);
    Serial.println("Sending " + message);
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
}

void sendMessage(String outgoing) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length

  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length()) {   // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }

  // if the recipient isn't this device or broadcast,
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // skip rest of function
  }

  // if message is for this device, or broadcast, print details:
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println("Message: " + incoming);
  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
}
```

پایه csPin برای انتخاب LoRa در ارتباط SPI بکار میرود (همانطور که میدانید در ارتباط SPI چندین ماژول که همگی Slave یا برده اند میتوانند به یک Master یا رئیس متصل شوند و Master توسط پین CS(Cheap Select) در هر زمان یک Slave را انتخاب میکند تا با هم حرف بزنند).

پایه resetPin هم بوضوح برای ریست کردن LoRa Ra بکار میرود.

پایه irqPin پایه ای است که میتوان تنظیم کرد تا LoRa موقع دریافت اطلاعات، روی این پایه سیگنال دهد و معمولا یکی از پایه های وقفه آردوینو را به عنوان irqPin انتخاب میکنیم تا موقع دریافت اطلاعات، روتین وقفه در آردوینو اجرا شود. اما در این مثال از وقفه استفاده نمیکنیم.

```cpp
if (!LoRa.begin(915E6)) {             // initialize ratio at 915 MHz
```

در خط بالا LoRa را برای فرکانس 915MHz تنظیم میکنیم که شما باید فرکانس مناسب فرستنده-گیرمنده خودتان را بنویسید که احتمالا 433MHz میباشد. اگر مشکلی در ارتباط وجود داشته باشد، پیام "LoRa init failed. Check your connections." را در سریال مانیتور آردوینو و در Baud Rate 9600 خواهید دید.

توجه کنید که اگر LoRa شما برای فرکانس خاصی میباشد، میتوانید در فرکانس های دیگر هم استفاده کنید اما ضعیف تر کار میکند. مثلا ماژول LoRa Ra-02 بنده در فرکانس 433MHz کار میکند ولی وقتی در فرکانس 915MHz از آن استفاده کردم جواب داد اما خیلی ضعیف تر عمل میکرد. ولی وقتی بجای 433MHz بر روی 434MHz تنظیم کردم بخوبی جواب میداد و تغییر فرکانس یکی از راه های ایجاد شبکه نیز میتواند باشد.

برای ارسال از تابع sendMessage استفاده شده است که در ابتدای آن با LoRa.beginPacket یک بسته ای از داده ها که قرار است فرستاده شود را شروع میکنیم.

سپس با LoRa.write محتویات پکیجمان را در آن مینویسیم. محتویات این بسته هرچیزی میتواند باشد و در روند ارسال تاثیری ندارد. مثلا در این مثال در ابتدای بسته، آدرسی را مینویسیم که میخواهیم بسته را برای آن بنویسیم (در ارسال اطلاعات هیچ تاثیری نمیگذارد. این آدرس را مینویسیم تا گیرنده بفهمد که آیا این بسته برای او فرستاده شده است ویا برای گیرنده دیگری است). سپس آدرس فرستنده را مینویسیم (تا گیرنده بفهمد که چه کسی این پیام را فرستاده است). سپس شماره پیام را مینویسیم که در هربار ارسال آن را یکی افزایش میدهیم. پس از آن طول پیام اصلی را مینویسیم. در آخر نیز پیام اصلی نوشته میشود.

با دستور LoRa.endPacket این بسته را تمام میکنیم و آن را میفرستیم.

در هربار اجرای loop، یکبار تابع onReceive اجرا میشود و LoRa.parsePacket که طول پیام دریافتی را برمیگرداند (اگر پیامی دریافت نشود مقدار 0 را برمیگرداند)، را به ورودی onReceive میدهیم.

در onReceive نیز از LoRa.read برای خواندن یک بایت دیتا (البته بصورت virtual int) استفاده میشود. برای خواندن پیام اصلی (فرمت آن String درنظر گرفته شده) یک String خالی میسازیم و دوباره با استفاده از LoRa.read بایت به بایت داده را میخوانیم و به عنوان char به String اضافه میکنیم. مابقی تابع onReceive برای چک کردن اطلاعات و نمایش اطلاعات میباشد.

## ایجاد شبکه محلی توسط LoRa

این هدف را خودمان باید محقق کنیم و مانند NRF24L01 نیست که خود چیپ چنین امکانی را برایمان محقق سازد. راه های مختلفی برای این منظور وجود دارد. حتی در مثال بالا نیز از یکی از این راه ها استفاده کردیم. یعنی: تعیین آدرس برای هرکدام از ماژول ها.
اما اگر بخواهیم لیستی از راه های موجود بنویسیم احتمالا به لیست زیر برسیم:
-	تعیین آدرس برای هر ماژول (ماژول ها، پیام های یکدیگر را دریافت میکنند اما میکروکنترلر وقتی میبیند برای گیرنده دیگریست، میتواند آنرا نادیده بگیرد. اما نکته اینجاست که پیام های غیر مربوط نیز میکرو را مشغول میکند و البته اگر بخواهیم از وقفه نیز استفاده کنیم راه حل خوبی نیست)
-	استفاده از فرکانس های مختلف (مثلا 420MHz تا 450MHz. بدین صورت که هرکدام از ماژول ها در یک فرکانس کار کند.)
-	استفاده از طول باند های مختلف
-	استفاده از SF های مختلف (گیرنده ها تنها پیام هایی که در فرکانس، طول باند و SF تنظیم شده آنها است را دریافت میکنند)

## شبکه LoRaWAN

لغت LoRaWAN به شبکه های LoRa اشاره دارد، و یک شبکه کلی از دستگاه های LoRa میسازد که میتوانند به اینترنت نیز متصل باشند  (البته میتوانید LoRaWAN را فراموش کنید و شبکه محلی خودتان را بسازید، با قوانینی که خودتان وضع میکنید و اگر نیاز داشتید اطلاعاتتان را به اینترنت منتقل کنید).

در این شبکه دستگاه های LoRa به Gateway ها متصل میشوند و Gateway ها نیز به اینترنت متصل اند و به اینصورت میتواند یک شبکه جهانی تشکیل شود.

یکی از نمونه های LoRaWAN ، [The Things Network][b9c4f9b4] می باشد. از طریق [لینک انجمن تهران در TheThingsNetwork][2603abd1] میتوانید اخبار تهران را در رابطه با The Things Network دنبال کنید. البته عجیب است که چند ماه پیش تعداد 4 Gateway در تهران ثبت شده بود اما الان (6/97) هیچ Gateway ای ثبت نیست! اما پیش بینی میشود که با حدود 10 Gateway درمکان های مناسب بتوان تهران را پوشش داد.

در لینک های مقابل میتوانید به اطلاعات کاملی در رابطه با LoRaWAN دست یابید:
-	[آموزش LoRaWAN][387efee7]
-	[آموزش دوم LoRaWAN][c4b82b90]

## مقایسه LoRa با SigFox

با اینکه این دو، مدولاسیون های کاملا مختلفی اند اما ازنظر برد پوششی و انرژی مصرفی LoRa و SigFox تا حد زیادی مشابه اند. اما نکته اصلی درمورد SigFox این است که تنها تحت شبکه خودش کار میکند. یعنی نمیتوان با تراشه های آن، شبکه محلی برای خودمان درست کنیم. اما همین امر باعث راحت شدن ایجاد ارتباط بین تراشه SigFox و شبکه سراسری آن میشود.

اما از معایب آن میتوانیم به موارد زیر اشاره کنیم:
1.	برای اتصال به شبکه آن باید هزینه بپردازیم
2.	مشکل اصلی آنها محدودیت شدید آن در تعداد پیام های ارسالی و دریافتی میباشد. این محدودیت بصورت زیر است:
-	4 * 8Byte – Download/Day  and  140 * 12Byte – Upload/Day
-	Speed Upto 100Byte/Day
3.	فرکانس آن نیز در هر ناحیه مقدار مشخصی است و باید در همان فرکانس کار کرد.

شرکت [ParsNet][44537cd4] در تهران پوشش دهی SigFox را بر عهده دارد.

## پروژه انجام شده بکمک LoRa

پروژه ای که در این رابطه انجام شد، نمایش دادن وضعیت دوربین ها از روی دستگاه – در یک شبکه محلی میباشد.

در این پروژه پس از تعیین وضعیت دوربین ها از روی دستگاه --، بکمک LoRa Ra-02 اطلاعات به دوربین ها انتقال پیدا میکرد و LED های مربوط به آنها روشن میشد.

اما این پروژه با دو مشکل مواجه شد که منجر به تغییر فرستنده-گیرنده آن از LoRa به NRF24L01 شد:
-	مشکل اول: با اینکه از رگولاتور AMS1117 3.3V برای تغزیه ماژول ها بکار میرفت و در نبود LoRa Ra-02 ولتاژ 3.3 ولت تولید میشد، اما وقتی ماژول LoRa در جای خود قرار میگرفت به شکل شگفت انگیزی شاهد افزایش ولتاژ(!) بودیم و ولتاژ به 4.2V نیز میرسید (احتمال داده میشد که مشکل از ماژول ها باشد که شاید آسیبی دیده باشند چرا که وقتی به 3.3V آردوینو هم میزدیم همین مشکل پابرجا بود). موقع ارسال پیام نیز تغییرات ولتاژ مشاهده میشد. با این حال ماژول ارسال اطلاعات را به درستی انجام میداد و به همین دلیل رفع این مشکل از اولویت خارج شد.
-	مشکل دوم: مشکل دوم پس از پس از گرانی هایی که در بازار قطعات الکترونیک رخ داد بوجود آمد و مشکل این بود که LoRa Ra-01/02 در بازار ایران بسختی یافت میشد و افزایش قیمت بسیار زیادی داشت. لذا تصمیم گرفته شد که از NRF24L01 در چنین پروژه ای استفاده شود.

## لینک ها

-	[My LoRa PlayList][31ebffa2]
-	[Semtech SX127x DataSheet and Resources][04a41e46]
-	[Adafruit learn LoRa and SigFox][5e330fdd]
-	[LoRa-Arduino Library (GitHub)][cabf28e5]
-	[Installing Additional Arduino Libraries][84f53b1d]


محمدامین حسن پور (Black3rror-HackerSpace)

12 شهریور 97

 

  [764309f3]: https://www.youtube.com/watch?v=adhWIo-7gr4&t=0s&list=PLagwPeY9ghvGWqt2uisneIfsqQeUt64Kl&index=6 "LoRa / LoRaWAN Range World Record Attempt. Will I succeed?"
  [7ccec0ce]: https://www.thethingsnetwork.org/article/ground-breaking-world-record-lorawan-packet-received-at-702-km-436-miles-distance "Ground breaking world record! LoRaWAN packet received at 702 km (436 miles) distance"
[1d0be94d]: https://www.youtube.com/watch?v=-d2JxZuvTOI&index=17&list=PLagwPeY9ghvGWqt2uisneIfsqQeUt64Kl&t=0s "What is LoRa?" 
  [ec4151cd]: https://www.youtube.com/watch?v=T3dGLqZrjIQ&index=18&list=PLagwPeY9ghvGWqt2uisneIfsqQeUt64Kl&t=5s "LoRa crash course by Thomas Telkamp"
[31ebffa2]: https://www.youtube.com/playlist?list=PLagwPeY9ghvGWqt2uisneIfsqQeUt64Kl "My LoRa PlayList"
  [04a41e46]: https://www.semtech.com/products/wireless-rf/lora-transceivers/SX1278#download-resources "Semtech SX127x DataSheet and Resources"
  [5e330fdd]: https://learn.adafruit.com/alltheiot-transports/lora-sigfox "Adafruit learn LoRa and SigFox"
  [1d0ca629]: https://fa.wikipedia.org/wiki/%D8%A8%DB%8C%D8%AA_%D8%AA%D9%88%D8%A7%D8%B2%D9%86 "بیت توازن"
[21d7c236]: https://youtu.be/8Oxcp9wQQnk?list=PLagwPeY9ghvGWqt2uisneIfsqQeUt64Kl&t=575 "Error Correction Rate"
[21d7c237]: https://fa.wikipedia.org/wiki/%DA%A9%D8%AF_%D9%85%D9%88%D8%B1%D8%B3 "کد مورس"
  [cabf28e5]: https://github.com/sandeepmistry/arduino-LoRa "arduino-LoRa"
  [84f53b1d]: https://www.arduino.cc/en/Guide/Libraries
  [b9c4f9b4]: https://www.thethingsnetwork.org/ "The Things Network"
  [2603abd1]: https://www.thethingsnetwork.org/community/tehran/ "لینک انجمن تهران در TheThingsNetwork"
  [387efee7]: https://www.thethingsnetwork.org/docs/lorawan/ "LoRaWAN Learning"
  [c4b82b90]: https://docs.exploratory.engineering/lora/ "LoRaWAN Learning"
  [44537cd4]: https://www.parsnet.io/ "ParsNet"
