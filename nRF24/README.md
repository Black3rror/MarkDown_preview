# nRF24L01+

## هدف این مقاله

در این مقاله قصد بر این است شما را با انواع ماژول های nRF24 آشنا کنیم و نحوه کار با آن ها را نیز توضیح دهیم.

## انواع nRF24

از لینک مقابل میتوانید انواع تراشه های 2.4GHz شرکت [Nordic Semiconductor][9b3b20e9] را مشاهده کنید که nRF24 نیز در همین فرکانس کار میکند: [لینک محصولات 2.4Ghz شرکت Nordic][f3ec0d69]

درضمن خوب است بدانیم که تراشه های nRF24 از [مدولاسیون GFSK][61e186e0] برای برقراری ارتباط استفاده میکنند.

همانطور که در تصویر پایین مشاهده میکنید، استفاده از چیپ های nRF2401A، nRF2402، nRF24L01، nRF24E1 و nRF24E2  وچند چیپ دیگر توصیه نمیشود.

![nRF not recommended](img/1.png)

### پس از چه چیپی استفاده کنیم؟

چیپ nRF24L01+ نمونه بهبود یافته nRF24L01 میباشد که علاوه بر نرخ انتقال داده 1Mbps و 2Mbps، نرخ 250Kbps نیز به آن اضافه شده است و همچنین حساسیت گیرنده آن ارتقا یافته است. البته اگر از نرخ انتقال داده 250Kbps در nRF24L01+ استفاده نکنید، با nRF24L01 سازگار بوده و میتوانند با هم ارتباط داشته باشند.

علاوه بر nRF24L01+، تراشه [nRF24LE1][e9ad86d5] نیز چیپ مناسبی میباشد. در این چیپ از پردازنده 16MHz 8-bit 8051 در کنار nRF24L01+ استفاده شده است  تا بتواند بطور مستقل عمل کند. در این تراشه همچنین از 1KB + 256B RAM و 16KB حافظه فلش استفاده شده است. امکانات این تراشه شامل اسیلاتور RC 16MHz و 32KHz و همچنین کریستال 32KHz، مبدل ADC دوازده بیتی و پروتکل های SPI، 2-Wire و UART نیز میشود. بنابراین احتمالا لازم نیست از میکرو دیگری درکنار nRF24LE1 استفاده کنید.

انواع بیشتری از nRF را میتوانید در جدول زیرین مشاهده کنید:

![nRF Series](img/2.png)

### ماژول های nRF24 موجود در بازار

در حال حاضر در بازار ایران nRF24L01 موجود هست که قرار شد بجای آن از نسخه پلاس استفاده کنیم: یعنی nRF24L01+ یا nRF24L01P. در فروشگاه ها شاید برد 100 تا 200 متر برای این ماژول ها درنظر گرفته شود ولی بدانید که این اعداد شاید در حالت ایده آل پاسخگو باشند ولی **خیلی** به وجود ویا عدم وجود موانع بستگی دارد. پس در محیط خانگی شاید مثلا کمتر از 50 متر برد داشته باشد (آزمایش انجام شده در پایان مقاله را بخوانید تا حس درست تری به قضیه داشته باشید). این برد ها معمولا آنتن روی برد دارند. همانند شکل زیر:

![nRF24L01+ module](img/11.jpg)

مورد بعدی که در فروشگاه های آنلاین امکان خوبی محسوب میشود، نروژی بودن چیپ است. شرکت NordicSemi یک کمپانی نروژی است پس احتمالا nRF24 ها هم در نروژ تولید شوند و نروژی بودن گواه از اوریجینال بودن است.

نمونه های دیگری که در فروشگاه ها میبینیم نمونه هایی اند که گفته میشود برد 1000m و یا 1100m دارند. ویا گفته میشود که از نوع nRF24L01+PA+LNA هستند. این ماژول ها دارای چیپ دیگری درکنار nRF هستند که قسمت PA+LNA را برعهدا دارد و واقعا هم برد را افزایش میدهد (اگر به برد توجه کنید، دارای 2 چیپ هستند). البته خود چیپ nRF نیز دارای PA/LNA است ولی برای افزایش برد پوششی، از چیپ دیگری درکنار آن استفاده میشود. این چیپ کار تقویت کردن سیگنال ارسالی و دریافتی را دارد. اطلاعات بیشتر درمورد اینکه PA/LNA چیست را از لینک مقابل دریافت کنید: [PA/LNA چیست][9e5ade73]. قطعا برد این ماژول ها هم در محیط خانگی بسار کمتر از 1000m است. دو نمونه از این ماژول ها را در زیر مشاهده میکنید:

![nRF24L01+PA+LNA module](img/12.jpg)

![nRF24L01+PA+LNA module](img/13.jpg)

## امکانات nRF24L01+

این تراشه دارای امکانات بسیاری میباشد که شاید برخی از آنها را از قلم بیاندازیم. توجه شود که حدودا تمام کاری که باید بکنیم تا از تراشه بهره ببریم این است که رجیستر های آن را طبق نیاز خود مقدار دهی کنیم و درآخر با دادن سیگنال مناسب به پایه CE از تراشه، خود تراشه کارها را برایمان انجام میدهد (ShockBurst فعال).

### enhanced ShockBurst

تراشه nRF میتواند از ShockBurst استفاده کند که یعنی خود تراشه کنترل روند ارسال و دریافت بسته های داده را بدست میگیرد و زمانبندی ها را بدقت دنبال میکند و کار Acknowledge گرفتن و ارسال مجدد را بصورت خود کار انجام میدهد. پس تنها کاری که ما باید انجام دهیم تنظیم رجیستر های nRF است تا طبق تنظیمات ما اطلاعات ارسال شوند و یا دریافت شوند. مثلا با نوشتن در یک رجیستر nRF میتوانیم طول داده را روی مقدار مشخصی تنظیم کنیم ویا طول آن را خود nRF بصورت خودکار تنظیم کند.

بسته های ارسالی زمانی که ShockBurst فعال است بشکل زیر میباشد.

![Packet](img/4.png)

در ابتدا یک بایت داده ارسال میشود تا گیرنده متوجه شود که داده هایی که دارند دریافت میشوند معتبرند و مثلا نویز هوا نیستند. برای مثال این یک بایت میتواند به شکل مقابل باشد: 01010101

سپس آدرس گیرنده میاید که در بخش PipeLine ها توضیح داده خواهد شد. طول این آدرس نیز توسط رجیستر مربوطه از 3 تا 5 بایت قابل تنظیم است.

بخش Packet control field نیز خودش به 3 بخش تقسیم میشود که میتوانید در تصویر زیر ببینید:

![Packet control field](img/5.png)

بخش Payload length آن وقتی امکان "طول داده دینامیک" فعال باشد بکار میرود و طول داده را میگوید و زمانی چنین امکانی غیرفعال باشد بی استفاده است.

قسمت PID نیز برای این بکار میرود که گیرنده متوجه شود که آیا بسته دریافتی جدید است ویا اینکه فرستنده تاییدیه بسته قبلی را دریافت نکرد و دوباره همان بسته قبلی را فرستاده است، که برای این منظور گیرنده از مقایسه CRC بسته دریافتی و بسته قبلی نیز استفاده میکند.

قسمت سوم که No Acknowledgement Flag است برای این بکار میرود که فرستنده به گیرنده بگوید که آیا لازم است سیگنال Acknowledge را بفرستد ویا خیر.

پس از Packet control field، داده اصلی میاید که میتواند از 0 تا 32 بایت باشد (پس اگر داده مان بیش از 32 بایت بود، باید در چند بسته بفرستیم).

لغت CRC مخفف Cyclic Redundancy Check است و برای تشخیص خطا بکار میرود، که از یک الگوریتم ریاضی برای این منظور استفاده میکند.

### Dynamic Payload Length

میتوانیم به تراشه nRF24 بگوییم که طول داده های ما مقدار مشخصی است (پس همان طول داده ای که فرستنده میفرستد باید درسمت گیرنده تنظیم شده باشد) ویا اینکه خود تراشه طول داده را در سمت فرستنده تشخیص دهد و همان طور که در بالا توضیح داده شد، طول آن را در بسته ارسالی بنویسد و گیرنده طول داده را از روی بسته بخواند و داده را بفهمد.

### auto Ack and retransmitting

امکان Acknowledge، امکان مفیدی است که در بسیاری از پروژه ها بکار میرود. بدین صورت که فرستنده پس از ارسال پیام منتظر جوابی از سوی گیرنده میماند تا مطمئن شود که گیرنده پیام را دریافت کرده (مثلا بنده حرفی به شما میزنم و شما با گفتن "شنیدم" به من خبر میدهید که پیامم را دریافت کردید). حال اگر پس از مدتی پیام acknowledgement دریافت نشد، فرستنده دوباره پیام را میفرستد (اگر "شنیدم" را دریافت نکردم، حرفم را تکرار میکنم) که امکان ارسال مجدد است. nRF میتواند هر دو کار را بصورت خودکار انجام دهد (مثلا در ماژول های LoRa این کار باید بصورت دستی انجام شود).

### using 6 PipeLines

امکان کاربردی دیگر nRF، استفاده از 6 پایپ لاین است که با این امکان میتوان به راحتی [شبکه ستاره ای][7a0f008b] ایجاد کرد. پایپ لاین ها خطوطی اند که داده از آنها وارد یا ارسال میشود. میتوانید آنها را به 6 دروازه تشبیه کنید که هر دروازه آدرس خاص خود را دارد و اگر داده ای بیاید، تنها در حالتی که آدرس یکی از دروازه ها را داشته باشد میتواند وارد دروازه شود واگرنه، دروازه آن داده را قبول نمیکند. تصویر زیر نشان میدهد که چگونه یک گیرنده (البته ماژول های nRF فرستنده-گیرنده اند اما در اینجا داریم از حالت گیرنده آن استفاده میکنیم) که با PRX نشان داده ایم، با 6 فرستنده که با PTXx نشان داده ایم ایجاد ارتباط کرده است. میبینیم که گیرنده با هر فرستنده روی یکی از پایپ لاین ها یا همان دروازه ها ارتباط برقرار میکند. 

![PipeLines](img/3.png)


اما توجه کنید که در یک زمان نمیتوان از چند پایپ لاین همزمان اطلاعات دریافت کرد. هر پایپ لاین هم دارای تنظیمات جداگانه ای میباشد شامل:
-	فعال یا غیرفعال کردن CRC
-	نوع کد شدن CRC
-	طول آدرس گیرنده
-	فرکانس کانال
-	نرخ انتقال داده
-	تقویت LNA

فعال کردن ویا غیر فعال کردن پایپ لاین ها نیز دراختیار ما است.

### Power Amplifier control

در nRF24 میتوانیم با انرژی های مختفی داده هایمان را بفرستیم که طبیعتا بر میزان انرژی مصرفی و میزان برد پوششی تراشه تاثیر میگذارد.

## ارتباط آردوینو با nRF24

این تراشه از ارتباط SPI استفاده میکند و از این طریق میتوان رجیستر های آن را مقداردهی کرد ویا آنها را خواند و همچنین میتوان به حافظه FIFO آن که برای نگهداری داده بکار میرود (یک حافظه برای داده دریافتی و یک حافظه برای داده ای که قرار است ارسال شود) دسترسی داشت.

### اتصالات ماژول nRF24L01+ با آردوینو

nRF24L01+ pin  |  Arduino Uno pin
--|--
VCC  |  3.3V
GND  |  GND
MOSI  |  11 (MOSI)
MISO  |  12 (MISO)
SCK  |  13 (SCK)
CE  |  9
CSN  |  10
IRQ  |  not required

![nRF24L01+ pinout](img/6.PNG)

طبق جدول بالا اتصالات را برقرار کنید. پایه IRQ از nRF در سه حالت فعال میشود:
-	زمانی که عملیات فرستادن اطلاعات با موفقیت انجام شود و فرستنده سیگنال Ack را دریافت کند
-	زمانی که فرستنده به تعدادی که برایش تعیین کردیم تلاش کند تا بسته را بفرستد اما موفق به دریافت تاییدیه نشود
-	زمانی گیرنده پیام معتبری را دریافت کند

پس اگر بخواهیم میتوانیم پایه IRQ تراشه مان را به پایه وقفه میکرو متصل کنیم تا مثلا موقع دریافت پیام، میکرو وارد روتین وقفه شود.

**توجه:**  ماژول nRF24L01+ نیز ممکن است با مشکل تغزیه مواجه شود. این مشکل برای بنده پیش آمد. یعنی موقع ارسال داده، nRF24L01+ به خوبی کار نمیکرد. مشکل بنده با قرار دادن ماژول nRF بر روی برد محافظ و رگولاتور آن که از لینک مقابل خریداری کردم برطرف شد: [لینک خرید برد محافظ و رگولاتور nRF24L01+][afedc5b3]

اما احتمالا اگر درکنار تغزیه ماژول nRF24 (تا حد امکان نزدیک به آن) خازن با مقدار مناسب (احتمالا خازن تانتالیوم 10uF)  قرار دهید جوابگو مشکلتان باشد. درضمن برای رفع این مشکل بهتر است PA ماژول nRF را با دستور زیر بر روی پایین ترین سطح قرار دهید تا با کمترین انرژی، اطلاعات را ارسال کند.

```cpp
radio.setPALevel(RF24_PA_MIN);
```

### کدنویسی آردوینو

اگر خودتان بخواهید با هر میکرویی با nRF24 ارتباط برقرار کنید باید از پروتکل SPI استفاده کنید و طبق نکاتی که در بخش "8.3 SPI operation" گفته میشود و دستورات بخش "8.3.1 SPI Commands" از DataSheet تراشه nRF24L01P کار را جلو ببرید و رجیستر هایی که در بخش "9.1 Register map table" آورده شده است را مقدار دهی کنید. لینک دانلود DataSheet: [nRF24L01P Preliminary Product Specification v1.0][e7910997]

اما ما برای این منظور از کتابخانه RF24 استفاده میکنیم که میتوانید از لینک مقابل دانلود کنید: [RF24 Library (GitHub)][44a9f2b0]

همچنین از لینک مقابل میتوانید توابع مختلفی که در این کتابخانه وجود دارد و توضیحات آنها را مشاهده کنید: [لینک توابع کتابخانه RF24][a70578eb]

پس از دانلود و نصب کتابخانه ([آموزش نصب کتابخانه در آردوینو][84f53b1d]) برنامه آردوینو خود را باز کنید(اگر قبلا باز بود آن را بسته و دوباره باز کنید تا کتابخانه جدید اضافه شود). سپس از طریق File > Examples > RF24 > GettingStarted میتوانید مثال ابتدایی برای ارتباط دوطرفه بین ماژول های nRF را باز کنید.  سپس برنامه را بر روی هر دو آردوینو خود آپلود کنید. وقتی  دو ترمینال باز کنیم (baud rate روی 57600 تنظیم شود)، آردوینو ها restart میشوند و هر دو به حالت گیرنده میروند (با دستور radio.startListening که در setup است). با نوشتن "t" در ترمینال، ماژول را به حالت فرستنده میبریم و با نوشتن "r" آن را به حالت گیرنده میبریم. اما طبق ویدئویی که در لینک زیر گذاشتم، میبینیم که وقتی یکی را به حالت فرستنده میبریم، پیام های ارسالی fail میشوند و گیرنده چیزی را دریافت نمیکند (کد قسمت setup کامل نیست). اما وقتی گیرنده را به حالت فرستنده میبریم و دوباره به حالت گیرنده بازمیگردانیم، مشکل رفع میشود. البته به نظر میرسد که هنوز در دریافت Acknowledge مشکل داریم و با اینکه ارتباط برقرار است ولی فرستنده پیام Ack را دریافت نمیکند.

[لینک کارکرد nRF24L01+ با کتابخانه RF24][efd45c24]

اما بنده از کد دیگری استفاده میکنم که کاملا درست عمل کرده. کد را در پایین آورده ام:

```cpp
#include <RF24.h>
#include <SPI.h>

RF24 radio(7, 8);
//RF24 radio(D3, D8);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
int role = 2;

void setup() {

  Serial.begin(9600);
  if (role == 1)
    Serial.println("Role : Transmitter");
  if (role == 2)
    Serial.println("Role : Receiver");

  Serial.println("PRESS 'T' to begin transmitting to the other node");

  radio.begin();
  radio.setRetries(15, 15);
  radio.setPALevel(RF24_PA_MIN);
  radio.enableDynamicPayloads();
  if (role == 1) {
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1, pipes[1]);
  }
  if (role == 2) {
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(1, pipes[0]);
  }
  radio.startListening();

  //radio.printDetails();
}

void loop() {

  if (role == 1) {
    radio.stopListening();

    unsigned long time = millis();
    Serial.print("Now sending ");
    Serial.print(time);
    bool ok = radio.write( &time, sizeof(unsigned long) );

    if (ok)
      Serial.print("    ok...");
    else
      Serial.print("    failed");

    //going to listening mode
    radio.startListening();

    unsigned long started_waiting_at = millis();
    bool timeout = false;
    while ( ! radio.available() && ! timeout )
      if (millis() - started_waiting_at > 200 )
        timeout = true;

    if ( timeout )
    {
      Serial.println("    Failed, response timed out.");
    }
    else
    {
      unsigned long got_time;
      radio.read( &got_time, sizeof(unsigned long) );

      Serial.print("    Got response ");
      Serial.print(got_time);
      Serial.print(" , round-trip delay: ");
      Serial.println( millis() - got_time );
    }

    delay(1000);
  }


  if (role == 2) {
    if ( radio.available() ) {

      unsigned long got_time;
      bool done = false;

      radio.read( &got_time, sizeof(unsigned long) );

      Serial.print("Got payload ");
      Serial.print(got_time);

      delay(20);

      //going to transmitter mode
      radio.stopListening();

      radio.write( &got_time, sizeof(unsigned long) );
      Serial.println("    Sent response");

      radio.startListening();

    }
  }


  if ( Serial.available() ) {

    char c = toupper(Serial.read());
    if ( c == 'T' && role == 2 )
    {
      Serial.println("*** CHANGING TO TRANSMIT ROLE -- PRESS 'R' TO SWITCH BACK");

      role = 1;
      radio.openWritingPipe(pipes[0]);
      radio.openReadingPipe(1, pipes[1]);
    }
    else if ( c == 'R' && role == 1 )
    {
      Serial.println("*** CHANGING TO RECEIVE ROLE -- PRESS 'T' TO SWITCH BACK");

      role = 2;
      radio.openWritingPipe(pipes[1]);
      radio.openReadingPipe(1, pipes[0]);
    }

  }

}
```

با دستور `RF24 radio(7, 8);` یک شیء از کلاس RF24 میسازیم و پایه های CE و CSN از nRF را به ترتیب روی پایه های 7 و 8 تنظیم میکنیم.

آرایه pipes نیز معرف آدرس PipeLine ها است که بعدا آنها را آدرس دهی میکنیم.

سپس تعداد بیشترین تلاش برای ارسال یک بسته داده را برابر 15 قرار میدهیم و میزان تاخیر میان تلاش های مجدد برای ارسال داده را نیز 15 (*250us) میکنیم. پس اگر داده ای را ارسال کرد و تاییدیه نگرفت، 15*250us صبر میکند و سپس دوباره تلاش میکند و حداکثر 15 بار این کار را انجام میدهد.

پس از آن قدرت PA را روی کمترین مقدار قرار میدهیم تا با مشکل افت ولتاژ مواجه نشویم.

سپس طول داده را نیز روی دینامیک قرار میدهیم تا بتوانیم داده هایی با طول های متفاوت ارسال کنیم.

حال با توجه به اینکه در حالت فرستنده یا گیرنده هستیم، آدرسی را برای ارسال دیتا مینویسیم (آدرس پایپ لاین گیرنده را در سمت فرستنده بنویسید) و PipeLine یک را نیز بعنوان پایپ لاین گیرنده آدرس دهی میکنیم.

سپس با دستور `radio.startListening();` در حالت گیرنده قرار میگیریم.

در قسمت loop:

#### در حالت فرستنده باشیم

با دستور `radio.stopListening();` از حالت گیرنده خارج میشویم.
در دستور `bool ok = radio.write( &time, sizeof(unsigned long) );` آدرس متغیری که میخواهیم اطلاعات آن ارسال شود را همراه با طولی که میخواهیم ارسال شود میدهیم و ماژول اطلاعات را ارسال میکند. مثلا دراینجا متغیری به نام time داریم که آدرس آن را میدهیم تا ارسال شود. نوع این متغیر نیز unsigned long است پس با ` sizeof(unsigned long)` طول متغیرمان را میدهیم تا nRF متغیر time را بدرستی ارسال کند. کار فرستادن تمام است و نتیجه آن در ok ذخیره شده.

مابقی داستان نوشتن نتیجه ارسال و رفتن به حالت گیرنده برای گرفتن پیام جدید میباشد (به Acknowledgement ارتباطی ندارد و میتوانستیم اصلا چنین کاری نکنیم).

#### در حالت گیرنده باشیم

هر بار با دستور ` if ( radio.available() ) {` بررسی میکنیم که آیا پیام جدیدی دریافت شده یا خیر.

اگر بله، وارد if میشود و با دستور ` radio.read( &got_time, sizeof(unsigned long) );` آدرس یک متغیر را میدهیم تا داده دریافتی را در آن بنویسد. البته حداکثر طول متغیرمان را میدهیم که بیش از ظرفیتی که متغیرمان دارد در آن ننویسد.

قسمت خواندن تمام است و مابقی داستان، نوشتن پیام دریافتی و ارسال پیامی جدید است (به Acknowledgement ارتباطی ندارد و میتوانستیم اصلا چنین کاری نکنیم).

## آزمایش برد پوششی nRF24L01+PA+LNA

### شرایط آزمایش

این آزمایش در فضای شرکت هکراسپیس و در مجموعه لارک انجام شد.

از 2 ماژول nRF24L01+ استفاده شد که روی آن نوشته شده: nRF24L01+ (XPower)  www.RFXPOWER.com  22 dbm

تصویر آنها را در شکل زیر مشاهده میکنید:

![nRF24L01+PA+LNA](img/7.jpg)

آردوینو درسمت گیرنده با یک پاور بانک 5V/2A (Max) تغزیه میشد و پایه 3.3V آن مستقیما به تغزیه ماژول nRF متصل بود. گیرنده بر روی سقف خودرو قرار داشت تا دید مستقیمی بر محیط داشته باشد. که فلزی بودن سقف خودرو میتواند مقداری در کیفیت آزمایش تاثیر بگذارد. تصویر زیر، گویای وضعیت گیرنده میباشد:

![Receiver Side](img/8.jpg)

آردوینو در سمت فرستنده به لپ تاپ متصل بود و فرستنده بر روی لپ تاپ و حدودا در ارتفاع 1.5m قرار گرفته بود. تصویر زیر، گویای وضعیت فرستنده میباشد:

![Transmitter Side](img/9.jpg)

اعدادی که ثبت شده اند، در حالتی بود که فرستنده و گیرنده دید مستقیم به یکدیگر داشتند و مانعی میان آنها نبود. اعداد ثبت شده نشان دهنده بیشترین فاصله میان فرستنده و گیرنده بود که ارتباط پایداری بین آن دو شکل گرفت (البته شاید قبل از این فاصله، شاهد قطعی ارتباط بودیم ولی در فاصله ثبت شده، ارتباط پایدار مشاهده میشد). این فواصل به جهت گیری  آنتن های فرستنده و گیرنده نیز بستگی دارد که سعی کردیم در بهترین حالت باشند. بیشترین فاصله مستقیم مجموعه حدود 200 متر است لذا فواصل بیشتر را تست نکردیم. تصویر زیر نشان دهنده بیشترین فاصله فرستنده از گیرنده است (حدود 200 متر) و دور خودرویی که گیرنده بر روی آن قرار دارد خط قرمزی کشیده شده است:

![Max Distance](img/10.jpg)

گفتن این نکته هم لازم است که فرکانس کاری آنتن های استفاده شده قطعی نیست و تا حدی احتمال دارد که بجای 2.4GHz، برای 5.8GHz ساخته شده باشند.

### برنامه آزمایش

در این آزمایش از Arduino UNO استفاده شد که در دو سمت گیرنده و فرستنده، برنامه زیر بر روی آنها ریخته شده بود:

```cpp
#include <RF24.h>
#include <SPI.h>

RF24 radio(7, 8);
//RF24 radio(D3, D8);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
int role = 2;

void setup() {

  Serial.begin(115200);
  if (role == 1)
    Serial.println("Role : Transmitter");
  if (role == 2)
    Serial.println("Role : Receiver");

  Serial.println("PRESS 'T' to begin transmitting to the other node");

  radio.begin();
  radio.setRetries(15, 15);
  radio.setPALevel(RF24_PA_MIN);
  radio.enableDynamicPayloads();
  if (role == 1) {
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1, pipes[1]);
  }
  if (role == 2) {
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(1, pipes[0]);
  }
  radio.startListening();

  //radio.printDetails();
}

void loop() {

  if (role == 1) {
    radio.stopListening();

    unsigned long time = millis();
    Serial.print("Now sending ");
    Serial.print(time);
    bool ok = radio.write( &time, sizeof(unsigned long) );

    if (ok)
      Serial.print("    ok...");
    else
      Serial.print("    failed");

    //going to listening mode
    radio.startListening();

    unsigned long started_waiting_at = millis();
    bool timeout = false;
    while ( ! radio.available() && ! timeout )
      if (millis() - started_waiting_at > 200 )
        timeout = true;

    if ( timeout )
    {
      Serial.println("    Failed, response timed out.");
    }
    else
    {
      unsigned long got_time;
      radio.read( &got_time, sizeof(unsigned long) );

      Serial.print("    Got response ");
      Serial.print(got_time);
      Serial.print(" , round-trip delay: ");
      Serial.println( millis() - got_time );
    }

    delay(1000);
  }


  if (role == 2) {
    if ( radio.available() ) {

      unsigned long got_time;
      bool done = false;

      radio.read( &got_time, sizeof(unsigned long) );

      Serial.print("Got payload ");
      Serial.print(got_time);

      switch(got_time) {
        case 1:
          Serial.print("PA_Level: ");
          radio.setPALevel(RF24_PA_MIN);
          Serial.println(radio.getPALevel());
          break;
        case 2:
          Serial.print("PA_Level: ");
          radio.setPALevel(RF24_PA_LOW);
          Serial.println(radio.getPALevel());
          break;
        case 3:
          Serial.print("PA_Level: ");
          radio.setPALevel(RF24_PA_HIGH);
          Serial.println(radio.getPALevel());
          break;
        case 4:
          Serial.print("PA_Level: ");
          radio.setPALevel(RF24_PA_MAX);
          Serial.println(radio.getPALevel());
          break;
        default:
          break;
      }

      delay(20);

      if (got_time > 4) {   // if it was PAcommand, dont response.
        //going to transmitter mode
        radio.stopListening();
  
        radio.write( &got_time, sizeof(unsigned long) );
        Serial.println("    Sent response");
  
        radio.startListening();
      }

    }
  }


  if ( Serial.available() ) {

    char c = Serial.read();
    if ( c == 't' && role == 2 )
    {
      Serial.println("*** CHANGING TO TRANSMIT ROLE -- PRESS 'R' TO SWITCH BACK");

      role = 1;
      radio.openWritingPipe(pipes[0]);
      radio.openReadingPipe(1, pipes[1]);
    }
    else if ( c == 'r' && role == 1 )
    {
      Serial.println("*** CHANGING TO RECEIVE ROLE -- PRESS 'T' TO SWITCH BACK");

      role = 2;
      radio.openWritingPipe(pipes[1]);
      radio.openReadingPipe(1, pipes[0]);
    }

    // PA commands ...............................
    else if ( c == 'm' && role == 1 )
    {
      Serial.print("PA_Level: ");
      radio.setPALevel(RF24_PA_MIN);
      Serial.println(radio.getPALevel());
    }
    else if ( c == 'l' && role == 1 )
    {
      Serial.print("PA_Level: ");
      radio.setPALevel(RF24_PA_LOW);
      Serial.println(radio.getPALevel());
    }
    else if ( c == 'h' && role == 1 )
    {
      Serial.print("PA_Level: ");
      radio.setPALevel(RF24_PA_HIGH);
      Serial.println(radio.getPALevel());
    }
    else if ( c == 'x' && role == 1 )
    {
      Serial.print("PA_Level: ");
      radio.setPALevel(RF24_PA_MAX);
      Serial.println(radio.getPALevel());
    }

    
    // send PA commands ..........................
    else if ( c == 'M' && role == 1 )
    {
      Serial.print("send PA_Level: min\t");
      radio.stopListening();
      unsigned long PAcommand = 1;
      Serial.println(radio.write( &PAcommand, sizeof(unsigned long) ));
    }
    else if ( c == 'L' && role == 1 )
    {
      Serial.print("send PA_Level: low\t");
      radio.stopListening();
      unsigned long PAcommand = 2;
      Serial.println(radio.write( &PAcommand, sizeof(unsigned long) ));
    }
    else if ( c == 'H' && role == 1 )
    {
      Serial.print("send PA_Level: high\t");
      radio.stopListening();
      unsigned long PAcommand = 3;
      Serial.println(radio.write( &PAcommand, sizeof(unsigned long) ));
    }
    else if ( c == 'X' && role == 1 )
    {
      Serial.print("send PA_Level: max\t");
      radio.stopListening();
      unsigned long PAcommand = 4;
      Serial.println(radio.write( &PAcommand, sizeof(unsigned long) ));
    }

  }

}
```

### نتایج آزمایش

هر ستر جدول نشان دهنده PA_Level در سمت فرستنده و گیرنده میباشد. هر ستون آن نیز نشان دهنده متصل بودن آنتن به nRF24 در سمت فرستنده (yes یا no در سمت چپ) و گیرنده (yes یا no در سمت راست) میباشد. فواصل ثبت شده به متر میباشند.

  | no-no | no-yes | yes-no | yes-yes 
--|---|---|---|--
min-min  | - | 120 | 108 | 200
low-low  | - | 148 | 120 | -
high-high  | - | 195 | 136 | -
max-max  | <10 | - | 140 | -

میبینیم که اگر هیچکدام از ماژول ها آنتن نداشته باشند، برد آنها شاید به کمتر از 10 متر برسد. پس حتما در پروژه هایتان از آنتن استفاده کنید (البته nRF هایی که PA+LNA ندارند، معمولا دارای آنتن روی برد میباشند).

نکته جالب و شگفت انگیز دیگر این است که آزمایش نشان میدهد که اگر بخواهید در سمت گیرنده ویا فرستنده از آنتن استفاده نکنید، بهتر است که گیرنده شما دارای آنتن باشد و فرستنده فاقد آنتن باشد. این نتیج هشگفت انگیز بود چراکه در هر بار ارسال، فرستنده اطلاعات را میفرستند و گیرنده دریافت میکند، اما گام بعدی این است که گیرنده، تاییدیه را بفرستد و فرستنده آن را دریافت کند. پس هر دو ماژول در حالت گیرنده و فرستنده میروند پس عوض کردن نقششان نباید تاثیری بر نتایج بگذارد. این آزماش حدود 2 بار انجام شد تا مطمئن شوم که برد پوششی کمتر شده است.

اما بهترین نتیجه زمانی گرفته میشود که هم فرستنده و هم گیرنده دارای آنتن باشند. زمانی که PA_Level هم گیرنده و هم فرستنده بر روی min بود، به فاصله 200 متر دست یافتیم و حالت های دیگر را نتوانستیم تست کنیم. اما حدس میزنیم که در بهترین حالت نیز به برد 350m نیز نرسیم.

این ماژول های nRF را در بازار با برد 1000m ویا 1100m میفروشند اما دیدیم که عملا با این اعداد بسیار فاصله داریم.

محمامین حسن پور (Black3rror-HackerSpace)

14 شهریور 97



 
  [9b3b20e9]: https://www.nordicsemi.com/eng "Nordic Semiconductor"
  [f3ec0d69]: https://www.nordicsemi.com/eng/Products/2.4GHz-RF "2.4GHz RF"
[e9ad86d5]: https://www.nordicsemi.com/eng/Products/2.4GHz-RF/nRF24LE1 "nRF24LE1"
[7a0f008b]: http://www.mahampardaz.com/ArticleDetails/network-topology "انواع شبکه ها"
[61e186e0]: https://en.wikipedia.org/wiki/Frequency-shift_keying "FSK Modulation"
[afedc5b3]: https://shop.aftabrayaneh.com/Nrf24l01_3_3V_Adapter_Board__AFTAB.html?search=nrf "برد محافظ و رگولاتور ماژول های NRF24L01P"
[44a9f2b0]: https://github.com/maniacbug/RF24 "RF24 Library"
[a70578eb]: http://maniacbug.github.io/RF24/classRF24.html#afb97dc4bdf4d2d84ea44060ac5b4ed89 "RF24 Class Reference"
[84f53b1d]: https://www.arduino.cc/en/Guide/Libraries
[efd45c24]: https://youtu.be/Uc9mQpq4New "Arduino nRF"
[9e5ade73]: https://electronics.stackexchange.com/questions/237267/what-is-a-pa-lna "What is PA/LNA"
[e7910997]: https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf "nRF24L01P Preliminary Product Specification v1.0"
